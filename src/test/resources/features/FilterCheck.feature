# new feature
# Tags: optional
    
Feature: Item filter check

@filter
Scenario Outline: Filter funcionality verification
    Given I have opened sportdirectpage
    And I have closed cookie pop-up
    And I have clicked on mens accessories button
    And I have selected footwear category in side menu
    And I have clicked on shoes button
    And I have selected Skechers brand
    And I have selected Firetrap brand
    And I have saved Skechers brand name
    And I have saved Firetrap brand name
    And I have entered price range from <min> to <max>
    When I press Go button
    Then Only corresponding brands should be filtered
    And Displayed prices should be in range from <min> to <max>

    Examples:
    | min | max |
    | 30  | 60  |



