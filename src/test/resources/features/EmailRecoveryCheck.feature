# new feature
# Tags: optional
    
Feature: Password recovery functionality check

@recovery
Scenario: Email recovery check
    Given I have opened sportdirectpage
    And I have clicked [Sign in] button
    And I have clicked on [Forgoten password] link
    And I have entered email address in recovery window
    And I have pressed [Send mail] button
    And I have checked that mail send acceptance message is displayed
    And I have opened password recovery url
    And I have entered new password
    And I have entered password into confirm field
    When I am pressing [Change password] button
    Then Password recovery confirm message is displayed





