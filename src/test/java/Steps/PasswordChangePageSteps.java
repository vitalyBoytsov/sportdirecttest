package Steps;

import Actions.Action;
import Actions.ActionObjectCreator;
import Actions.Navigation;
import Locators.PasswordChangePageLocators;
import MailManager.emailChecker;
import TestData.TestData;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class PasswordChangePageSteps extends ActionObjectCreator {

    @Given("I have opened password recovery url")
    public void openPwdRecoveryUrl() throws Throwable{
        String url = text.getUrlFromText(emailChecker.getLatestUnseenMessage(TestData.getData("emailSD")));
        navigation.goToUrl(text.getUrlFromText(url));
    }

    @Given("I have entered new password")
    public void enterNewPassword() throws Throwable{
        action.enterText(PasswordChangePageLocators.newPasswordInput,TestData.getData("emailPwd"));
    }

    @Given("I have entered password into confirm field")
    public void enterConfirmPassword() throws Throwable{
        action.enterText(PasswordChangePageLocators.confirmPasswordInput,TestData.getData("emailPwd"));
    }

    @When("I am pressing \\[Change password] button")
    public void changePwdPress() throws Throwable{
        action.click(PasswordChangePageLocators.passwordChangeBtn);
    }

    @Then("Password recovery confirm message is displayed")
    public void confirmMessageCheck() throws Throwable{
        Assert.assertEquals(TestData.getData("recovrMsg"),
                text.getText(PasswordChangePageLocators.pwdChangeSuccessTxt));
    }
}
