package Steps;

import Actions.ActionObjectCreator;
import Locators.PasswordRecoveryLocators;
import TestData.TestData;
import cucumber.api.java.en.Given;
import org.junit.Assert;

public class PasswordRecoveryPageSteps extends ActionObjectCreator {

    @Given("I have entered email address in recovery window")
    public void enterEmail() throws Throwable{
        action.enterText(PasswordRecoveryLocators.emailInput, TestData.getData("emailUsr"));
    }

    @Given("I have pressed \\[Send mail] button")
    public void pressSendMail() throws Throwable{
        action.click(PasswordRecoveryLocators.sendEmailBtn);
    }

    @Given("I have checked that mail send acceptance message is displayed")
    public void checkAcceptMessage() throws Throwable{
        Assert.assertEquals(TestData.getData("acceptMsg"), text.getText(PasswordRecoveryLocators.acceptMessage));
    }
}
