package Steps;

import Actions.Action;
import Actions.ActionObjectCreator;
import Locators.SignInPageLocators;
import cucumber.api.java.en.Given;

public class SignInPageSteps extends ActionObjectCreator {

    @Given("I have clicked on \\[Forgoten password] link")
    public void forgotLinkClick() throws Throwable{
        action.click(SignInPageLocators.forgotPwdLink);
    }
}
