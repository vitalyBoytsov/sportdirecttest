package Steps;

import Actions.Action;
import Actions.ActionObjectCreator;
import Actions.Wait;
import Locators.MensShoesPageLocators;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;

public class MensShoesPageSteps extends ActionObjectCreator {

    public List<String> _brands = new ArrayList<String>();

    @Given("I have selected Skechers brand")
    public void selectSkechersBrand() throws Throwable{
        action.click(MensShoesPageLocators.skechersBrandSelect);
    }

    @Given("I have selected Firetrap brand")
    public void selectFiretrapBrand() throws Throwable{
        action.click(MensShoesPageLocators.firetrapBrandSelect);
    }

    @Given("I have saved (.*) brand name")
    public void saveBrand(String brand) throws Throwable{
        _brands.add(brand);
    }

    @When("I press Go button")
    public void goButtonPress() throws Throwable{
        action.javascriptClick(MensShoesPageLocators.goButton);
    }

    @Given("I have entered price range from (.*) to (.*)")
    public void priceRangeEnter(String min, String max) throws Throwable{
        action.enterText(MensShoesPageLocators.minPriceInput, min);
        action.enterText(MensShoesPageLocators.maxPriceInput, max);
    }

    @Then("Only corresponding brands should be filtered")
    public void brandsFilterCheck(){
        wait.waitUntilStaleExceptEnd(MensShoesPageLocators.selectedShoesNames);
        Assert.assertTrue(check.checkElementListCorrectnes(MensShoesPageLocators.selectedShoesNames, _brands));
        if(action.getElementCount(MensShoesPageLocators.selectedShoesNames)>100){
            action.click(MensShoesPageLocators.nextButtom);
            brandsFilterCheck();
        }
    }

    @Given("Displayed prices should be in range from (.*) to (.*)")
    public void pricesCheck(String min, String max){
        Double minPage = action.getMinDouble(action.getListOfDoublesFromString
                (text.getMoneyWithoutCurrency(MensShoesPageLocators.selectedShoesPrices)));
        Double maxPage = action.getMaxDouble(action.getListOfDoublesFromString
                (text.getMoneyWithoutCurrency(MensShoesPageLocators.selectedShoesPrices)));
        Assert.assertTrue(maxPage<=Double.parseDouble(max));
        Assert.assertTrue(minPage>=Double.parseDouble(min));
    }
}
