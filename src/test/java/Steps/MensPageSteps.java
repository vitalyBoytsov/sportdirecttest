package Steps;

import Actions.ActionObjectCreator;
import Locators.MensPageLocators;
import Locators.StartPageLocators;
import cucumber.api.java.en.Given;

public class MensPageSteps extends ActionObjectCreator {

    @Given("I have clicked on shoes button")
    public void clickMensShoesButton() throws Throwable{
        action.click(MensPageLocators.shoesBtn);
    }

    @Given("I have selected footwear category in side menu")
    public void clickFootwearCategory() throws Throwable{
        action.click(MensPageLocators.footwearBtn);
    }
}
