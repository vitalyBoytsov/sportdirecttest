package Steps;

import Actions.Action;
import Actions.ActionObjectCreator;
import Actions.Navigation;
import Locators.MensPageLocators;
import Locators.StartPageLocators;
import TestData.TestData;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Given;


public class StartPageSteps extends ActionObjectCreator {


    @Given("I have opened sportdirectpage")
    public void openSportDirectPage() throws Throwable{
        navigation.goToUrl(TestData.getData("url").toString());
    }

    @Given("I have clicked on mens accessories button")
    public void clickMensButton() throws Throwable {
        action.multipleClick(StartPageLocators.mensAccessories, 2);
    }

    @Given("I have clicked on start pages mens shoes button")
    public void clickMensShoesButton() throws Throwable{
        action.click(StartPageLocators.mensShoesButton);
    }

    @Given("I have clicked \\[Sign in] button")
    public void signInClick() throws Throwable {
        action.click(StartPageLocators.signInBtn);
    }

    @Given("I have closed cookie pop-up")
    public void closeCookie() throws Throwable {
        action.click(StartPageLocators.cookiePopup);
    }


}
