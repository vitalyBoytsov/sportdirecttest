import Actions.ActionObjectCreator;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;


public class Hooks {

    public static WebDriver driver;

    @Before
    public void before() {
        ChromeDriverManager.getInstance().setup();
        driver = new ChromeDriver();
        ActionObjectCreator.driverInitializer(driver);
        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
    }

    @After
    public void after(Scenario scenario){
        takeScreenshot(scenario);
        if(driver != null )driver.quit();
        ActionObjectCreator.driverDiscard();
    }

    private void takeScreenshot(Scenario scenario){
        if(scenario.isFailed()) {
            try {
                scenario.write("Current Page URL is " + driver.getCurrentUrl());
                byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png");
            } catch (WebDriverException somePlatformsDontSupportScreenshots) {
                System.err.println(somePlatformsDontSupportScreenshots.getMessage());
            }

        }
    }

}
