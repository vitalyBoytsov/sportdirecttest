package Locators;

import org.openqa.selenium.By;

public class PasswordChangePageLocators {

    public static By newPasswordInput = By.cssSelector("input[name*='txtNewPassword']");
    public static By confirmPasswordInput = By.cssSelector("input[name*='ConfirmNewPassword']");
    public static By passwordChangeBtn = By.cssSelector("a[id*='ChangePassword']");
    public static By pwdChangeSuccessTxt = By.cssSelector("[id*='PasswordReset_SuccessText']");
}
