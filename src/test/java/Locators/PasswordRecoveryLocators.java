package Locators;

import org.openqa.selenium.By;

public class PasswordRecoveryLocators {

    public static By emailInput = By.cssSelector("[name*='PasswordReset']");
    public static By sendEmailBtn = By.cssSelector("[id*='cmdSendPassword']");
    public static By acceptMessage = By.cssSelector("[class*='FormSuccess'] span");
}
