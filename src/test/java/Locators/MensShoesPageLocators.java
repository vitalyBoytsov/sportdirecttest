package Locators;

import org.openqa.selenium.By;

public class MensShoesPageLocators {

    public static By firetrapBrandSelect = By.cssSelector("[data-productname='Firetrap'] span[role='checkbox']");
    public static By skechersBrandSelect = By.cssSelector("[data-productname='Skechers'] span[role='checkbox']");
    public static By minPriceInput = By.cssSelector("[id='PriceFilterTextEntry'] [id*='Min']");
    public static By maxPriceInput = By.cssSelector("[id='PriceFilterTextEntry'] [id*='Max']");
    public static By goButton = By.cssSelector("[id='PriceFilterTextEntry'] [value='Go']");
    public static By selectedShoesNames = By.cssSelector(
            "[id='productlistcontainer'] [class='productdescriptionbrand']");
    public static By selectedShoesPrices = By.cssSelector("[id='productlistcontainer'] [class*='curprice']");
    public static By nextButtom= By.cssSelector("[class*='bottom'] [class*='NextLink']");
}
