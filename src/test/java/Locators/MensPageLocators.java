package Locators;

import org.openqa.selenium.By;

public class MensPageLocators {

    public static By footwearBtn = By.cssSelector("[id='mobNavigation'] a[href='#footwear']");
    public static By shoesBtn = By.cssSelector("[id='footwear'] [href*='mens-shoes']");
}
