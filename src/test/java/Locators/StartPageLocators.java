package Locators;

import org.openqa.selenium.By;

public class StartPageLocators {

    public static By mensAccessories = By.cssSelector("[id='topMenu'] [class*='GroupA']");
    public static By mensShoesButton = By.cssSelector("[class='Mens'] a[href*='mens-shoes']");
    public static By signInBtn = By.cssSelector(".TopLinkMenu [id*='loginLink']");
    public static By cookiePopup = By.cssSelector("[id='cookieContentContainer'] [value='X']");
}
