package MailManager;

import TestData.TestData;

import java.time.LocalDateTime;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.search.FromTerm;
import javax.mail.search.SearchTerm;

public class emailChecker {

    public static  String getLatestUnseenMessage(String from){
        String hostval = TestData.getData("emailHost");
        String uname = TestData.getData("emailUsr");
        String pwd = TestData.getData("emailPwd");
        List<String> response = new ArrayList<>();
        Integer timeIncr = Integer.parseInt(TestData.getData("emailWait"));
        LocalDateTime limit = LocalDateTime.now().plusSeconds(timeIncr);
        do {
            response = getUnseenInboxMessages(hostval,uname, pwd, from);
        }while (response.size()==0 && LocalDateTime.now().compareTo(limit)<0);
        if(response.size()==0) throw new ExceptionInInitializerError("No inbox messages found");
        return response.get(response.size()-1);
    }

    private static  List<String> getUnseenInboxMessages(String hostval, String uname,String pwd, String from)
    {
        List<String> result = new ArrayList<>();

        try {
            Properties propvals = getProperties(hostval);
            Session emailSessionObj = getSessionObject(propvals, uname, pwd);
            Store storeObj = emailSessionObj.getStore("pop3s");
            storeObj.connect(hostval, uname, pwd);
            Folder emailFolderObj = storeObj.getFolder("INBOX");
            emailFolderObj.open(Folder.READ_ONLY);
            //messageobjs = emailFolderObj.getMessages();
            SearchTerm sender = new FromTerm(new InternetAddress(from));
            Message[] messageobjs = emailFolderObj.search(sender);
            for (Message m:messageobjs){
                Object obj = m.getContent();
                Multipart mp = (Multipart)obj;
                BodyPart bp = mp.getBodyPart(0);
                result.add(bp.getContent().toString());
            }

            emailFolderObj.close(false);
            storeObj.close();

        } catch (NoSuchProviderException exp) {
            exp.printStackTrace();
        } catch (MessagingException exp) {
            exp.printStackTrace();
        } catch (Exception exp) {
            exp.printStackTrace();
        }
         return result;
    }

    private static Properties getProperties(String hostval){
        Properties properties = new Properties();
        properties.put("mail.pop3.host", hostval);
        properties.put("mail.pop3.port", "995");
        properties.put("mail.pop3.starttls.enable", "true");
        return properties;
    }

    private static Session getSessionObject(Properties properties, String usr, String pwd){
        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(usr, pwd);
            }
        });
        return session;
    }
}



