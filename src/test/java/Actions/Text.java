package Actions;

import TestData.TestData;
import jdk.nashorn.internal.runtime.regexp.joni.Regex;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Text {

    WebDriver driver;

    public Text(WebDriver _driver){
        driver=_driver;
    }

    public List<String> getElementsText(By by){
        new Wait(driver).waitUntilElementIsVisible(by);
        List<String>  result = new ArrayList<String>();
        for (WebElement el: driver.findElements(by)){
            result.add(el.getText());
        }
        return result;
    }

    public List<String> getMoneyWithoutCurrency(By by){
        new Wait(driver).waitUntilElementIsVisible(by);
        List<String> result = new ArrayList<String>();
        for (String i: getElementsText(by)){
            result.add(i.replace(" ","").replace(",",".")
                    .replace("€","").replace("$",""));
        }
        return result;
        }

    public String getUrlFromText(String text){
        Pattern p = Pattern.compile(TestData.getData("urlRegEx"));
        Matcher m = p.matcher(text);
        m.find();
        return m.group();
    }

    public String getText(By by){
        new Wait(driver).waitUntilElementIsVisible(by);
        return driver.findElement(by).getText();
    }




}
