package Actions;

import org.openqa.selenium.WebDriver;

public class ActionObjectCreator {
    private static WebDriver driver;

    public static WebDriver driverInitializer(WebDriver _driver) {
        if (driver == null) {
            driver = _driver;
            return driver;
        }
        return driver;
    }

    public static void driverDiscard(){
        driver = null;
    }

    public Navigation navigation = new Navigation(driver);
    public Action action = new Action(driver);
    public Check check = new Check(driver);
    public Text text = new Text(driver);
    public Wait wait = new Wait(driver);
}







