package Actions;

import TestData.TestData;
import org.apache.log4j.helpers.DateTimeDateFormat;
import org.apache.poi.ss.formula.functions.T;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Date;
import java.util.List;


public class Wait {

    private WebDriver driver;

    public Wait(WebDriver _driver) {
        driver = _driver;
    }

    public void waitForPageToLoad() {
        new WebDriverWait(driver, Integer.parseInt(TestData.getData("pageWait"))).until(driver ->
                ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete"));
    }

    public void waitUntilElementIsVisible(By by) {
        new WebDriverWait(driver,
                Integer.parseInt(TestData.getData("wait")))
                .until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public void waitUntilElementIsVisible(WebElement el) {
        new WebDriverWait(driver,
                Integer.parseInt(TestData.getData("wait"))).until(ExpectedConditions.visibilityOf(el));
    }

    public void waitUntilElementIsClickable(By by) {
        new WebDriverWait(driver,
                Integer.parseInt(TestData.getData("wait"))).until(ExpectedConditions.elementToBeClickable(by));
    }

    public void waitUntilElementEnabled(WebElement el) {
        new WebDriverWait(driver, Integer.parseInt(TestData.getData("wait"))).until(d -> el.isEnabled());
    }

    public void waitUntilElementEnabled(By by) {
        WebElement el = driver.findElement(by);
        new WebDriverWait(driver, Integer.parseInt(TestData.getData("wait"))).until(d -> el.isEnabled());
    }

    public void waitUntilStaleExceptEnd(By by){
        WebElement el = driver.findElements(by).get(0);
        int index = Integer.parseInt(TestData.getData("wait"));
        try {
            el.getText();
            Thread.sleep(500);
        }
        catch (StaleElementReferenceException e){
            if(index==0) return;
            index--;
            waitUntilStaleExceptEnd(by);
        }
        catch (Exception e){return;}
    }

}
