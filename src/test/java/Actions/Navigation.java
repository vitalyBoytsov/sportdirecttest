package Actions;

import org.openqa.selenium.WebDriver;

public class Navigation  {

    private WebDriver driver;

    public Navigation(WebDriver _driver){
        driver = _driver;
    }

    public void goToUrl(String url){
        new Wait(driver).waitForPageToLoad();
        driver.get(url);
    }
}
