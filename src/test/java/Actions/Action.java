package Actions;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

import java.security.Key;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Action {

    private WebDriver driver;

    public Action(WebDriver _driver) {
        driver = _driver;
    }

    public void click(By by) {
        new Wait(driver).waitUntilElementIsVisible(by);
        new Wait(driver).waitUntilElementIsClickable(by);
        new Wait(driver).waitUntilElementEnabled(by);
        driver.findElement(by).click();
    }

    public void doubleClick(By by) {
        new Wait(driver).waitUntilElementIsVisible(by);
        new Wait(driver).waitUntilElementIsClickable(by);
        new Wait(driver).waitUntilElementEnabled(by);
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(by)).doubleClick();
    }

    public void multipleClick(By by, int count) {
        new Wait(driver).waitUntilElementIsVisible(by);
        new Wait(driver).waitUntilElementIsClickable(by);
        new Wait(driver).waitUntilElementEnabled(by);

        do {
            driver.findElement(by).click();
            count--;
        }while (count>0&&count!=0);
    }

    public void moveClick(By by){
        new Wait(driver).waitUntilElementIsVisible(by);
        new Wait(driver).waitUntilElementIsClickable(by);
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(by)).click().build().perform();
    }

    public void enterText(By by, String txt) {
        new Wait(driver).waitUntilElementIsVisible(by);
        driver.findElement(by).sendKeys(txt);
    }

    public void pressEnter(By by){
        new Wait(driver).waitUntilElementIsVisible(by);
        new Wait(driver).waitUntilElementEnabled(by);
        Actions action = new Actions(driver);
        action.moveToElement(driver.findElement(by)).sendKeys(Keys.ENTER);
     }

    public void javascriptClick(By by) {
        new Wait(driver).waitUntilElementIsVisible(by);
        WebElement elem = driver.findElement(by);
        JavascriptExecutor exec = (JavascriptExecutor) driver;
        exec.executeScript("arguments[0].click();", elem);
    }

    public int getElementCount(By by) {
        new Wait(driver).waitUntilElementIsVisible(by);
        List<WebElement> elements = driver.findElements(by);
        return elements.size();
    }

    public List<WebElement> getElements(By by){
        List<WebElement> list = driver.findElements(by);
        return list;
    }

    public List<Double> getListOfDoublesFromString(List<String> stringList) {
        List<Double> result = new ArrayList<Double>();
        for (String i : stringList
                ) {
            result.add(Double.parseDouble(i));
        }
        return result;
    }

    public Double getMinDouble(List<Double> list) {
        return Collections.min(list);
    }

    public Double getMaxDouble(List<Double> list) {
        return Collections.max(list);
    }
}
