package Actions;

import org.openqa.selenium.*;

import java.util.List;

public class Check {

    WebDriver driver;

    public Check(WebDriver _driver){
        driver = _driver;
    }

    public Boolean checkElementListCorrectnes(By by, List<String> list){
        new Wait(driver).waitUntilElementIsVisible(by);
        List<WebElement> elList = driver.findElements(by);
        Boolean result = true;
        for (WebElement el: elList){
            new Wait(driver).waitUntilElementEnabled(el);
            if(!list.contains(el.getText())){
                result = false;
                break;
            }
        }
        return  result;
    }
}
