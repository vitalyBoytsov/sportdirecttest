package TestData;

import org.omg.CORBA.Object;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class TestData {

    public static  String getData(String key){
        String result = null;
        try{
            Yaml yaml = new Yaml();
            File file = new File("testData.yaml");
            System.out.println(file.getAbsolutePath());
            InputStream ios = new FileInputStream(file);
            Map<String, Object> object=  (Map<String, Object>) yaml.load(ios);
            result = getTestData(object, key);
            ios.close();

        }
        catch(Exception e){e.printStackTrace();}
        return result;
    }

    private static String getTestData(Map<String, Object>  obj, String key)  {
        String result = null;
        for (String k:obj.keySet()){
            if(k.equals(key)){
                result = String.valueOf(obj.get(k));
            }
        }
            if(result!=null)return result;
            throw new ExceptionInInitializerError("Test data +"+key+" not found in testData.yaml");
     }
}
